import React from 'react';
import homeOutline from '@iconify/icons-typcn/home-outline';

import Slider from './../components/Slider';
import SEO from './../components/Seo';
import Vide from './../components/Vide';
import Home from './../components/Home';
import Iphone from './../components/Iphone';
import Android from './../components/Android';
import Contact from './../components/Contact';

const components = [
  { slug: 'home', component: Home, name: 'Sún Ka', icon: homeOutline },
  { slug: 'iphone', component: Iphone, name: 'Mở khoá iPhone', icon: 'ls:iphone' },
  { slug: 'android', component: Android, name: 'Mở khoá Android', icon: 'ant-design:android-outlined' },
  { slug: 'map', component: Contact, name: 'Địa điểm', icon: 'akar-icons:location' },
];

export default () => (
  <>
    <SEO title="Unlock Android và iPhone" />
    <Vide url={'https://res.cloudinary.com/diliidcs0/video/upload/v1638766129/unlock/intro.mp4'} />
    <Slider components={components} />
  </>
);
