import React, { useEffect } from "react";
import { CustomChat } from "react-facebook";

const App = ({ children }) => {
  useEffect(function() {
    //
  }, [])

  return (
    <div className="app">
      {children}
      <CustomChat pageId={process.env.FACEBOOK_SUNKA_PAGE_ID} minimized={false} />
    </div>
  )
}

export default App;
