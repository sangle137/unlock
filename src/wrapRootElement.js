import React from "react";
import { FacebookProvider } from "react-facebook";

import App from "./App";

export default ({ element }) => {
  return (
    <FacebookProvider version={`v12.0`} appId={process.env.FACEBOOK_APP_ID} chatSupport>
      <App>{element}</App>
    </FacebookProvider>
  )
}
