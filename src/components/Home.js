import React from "react"

const Home = ({ containerClass }) => {
  return (
    <li className={containerClass}>
      <div className="cd-half-block content fullwidth-block overlay bgVideo">
        <div className="content-wrapper top-overlay">
          <div className="content-inner">
            <div>
              <div className="container">
                <div className="row">
                  <div className="col-xs-12 full-width">
                    <div className="content-container animated fadeIn">
                      {/* <div className="logo">
                        <a href="/"><img
                          src="https://res.cloudinary.com/diliidcs0/image/upload/w_200/v1598586640/logo_new.png"
                          alt="logo" /></a>
                      </div> */}
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
        <div className="cover-overlay" data-opacity="0.6" style={{ opacity: .5 }} />
      </div>
    </li>
  )
}

export default Home;
