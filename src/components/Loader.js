import React from 'react';
import { Modal, Spinner } from 'react-bootstrap';

function Loader(props) {
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    </Modal>
  );
}

export default Loader;
