import React, { Component } from "react"

class Layout extends Component {
  componentDidMount() {
    try {
      // this.UIkit = require('uikit/dist/js/uikit')
      // this.Icons = require('uikit/dist/js/uikit-icons')
      // this.UIkit.use(this.Icons)
    } catch (e) {
      console.error(e)
    }
  }

  render() {
    const { children } = this.props;
    return (
      <div className="container">
        {children}
      </div>
    )
  }
}

export default Layout;
