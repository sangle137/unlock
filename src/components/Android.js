import React from 'react'

const Android = ({ containerClass, slug }) => (
  <li className={containerClass}>
    <div
      className="cd-half-block image product-section product-layout-4"
      style={{ background: '#fff' }}
    >
      <div className="product-group">
        <div className="product-item">
          <div className="product-thumbnail bgImage" />
          <div className="product-details">
            <div className="price">{10}<span>Vnd</span></div>
            <div className="product-details-wrapper">
              <h3 className="product-title">Test</h3>
              <div className="product-desc" dangerouslySetInnerHTML={{ __html: '' }} />

              <div className="button button-sm button-border button-icon">
                <a target="_blank" href="https://www.now.vn/ho-chi-minh/sun-coffee-milk-tea">
                  <span className="typcn typcn-shopping-cart" />Đặt hàng
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="cd-half-block content" data-bg-color="#820333" style={{ backgroundColor: '#820333' }}>
      <div className="content-wrapper">
        <div className="content-inner">
          <div className="disable-vertical-align">
            <img src="https://res.cloudinary.com/diliidcs0/image/upload/w_100/v1598586640/logo_new.png" alt="logo" />
            <div data-height="68" style={{ height: '68px' }} />
            <h2 className="half-section-title">Sún Ka</h2>
            <div className="half-section-subtitle font-uppercase" style={{ marginBottom: '15px', letterSpacing: '4px' }}>
              Mì Quảng đậm chất Quảng
            </div>
            <div data-color="#f5f5f5" style={{ color: '#f5f5f5' }}>
              <blockquote style={{ fontSize: '1.2em' }}>
                <p>
                  Thương nhau múc bát chè xanh<br />
                  Làm tô mì quảng mời anh xơi cùng
                </p>
              </blockquote>
              <div>
                <p>Mì Quảng là một món ăn đặc trưng của miền Trung là Quảng Ngãi, Quảng Nam và Đà Nẵng , Việt Nam.</p>
                <p>Thường được làm từ sợi mì bằng bột gạo xay mịn và tráng thành từng lớp bánh mỏng, sau đó thái theo chiều ngang để có những sợi mì mỏng khoảng 2mm. Sợi mì làm bằng bột gạo được trộn thêm một số phụ gia cho đạt độ giòn, dai.</p>
                <p>Kế đến, đem chần mì qua nước sôi. Trong lúc chần, người thợ có quyết thêm dầu lạc để sợi mì không dính, đó là nguồn gốc cho vị béo đặc trưng của sợi mì Quảng</p>
                <p>Dưới lớp mì là các loại rau sống, Mì Quảng phải ăn kèm với rau sống 9 vị thì mới tạo nên được hương vị nồng nàn: húng quế, xà lách tươi, cải non mới nụ, giá trắng có thể được trụng chín hoặc để sống, ngò rí , rau răm với hành hoa thái nhỏ và thêm hoa chuối cắt mỏng.</p>
              </div>
              <div data-height="37" style={{ height: '37px' }} />
              <div className="half-section-subtitle font-uppercase" style={{ marginBottom: '15px', letterSpacing: '4px' }}>
                Đặt hàng ngay để thưởng thức
              </div>
              <ul className="delivery-list">
                <li className="delivery-item">
                  <a target="_blank" href="https://www.now.vn/ho-chi-minh/sun-coffee-milk-tea">
                    <img src="https://res.cloudinary.com/diliidcs0/image/upload/c_scale,w_auto,h_45,q_100/v1595575448/now-logo.jpg" alt="Now Food" />
                  </a>
                </li>
                <li className="delivery-item">
                  <a target="_blank" href="https://grab.onelink.me/2695613898?pid=instagram_organic&c=VN_GF_40520_instagram_organic_VNIGSS&is_retargeting=true&af_dp=grab%3A%2F%2Fopen%3FscreenType%3DGRABFOOD%26searchParameter%3DTrà%20Sữa%20Sún%20Ka&af_web_dp=https%3A%2F%2Fwww.grab.com%2Ffood&af_ios_url=https%3A%2F%2Fwww.grab.com%2Ffood&af_force_deeplink=true">
                    <img src="https://res.cloudinary.com/diliidcs0/image/upload/c_scale,w_auto,h_45,q_100/v1579153866/grab-logo.png" alt="Grab Food" />
                  </a>
                </li>
                <li className="delivery-item">
                  <a target="_blank" href="#">
                    <img src="https://res.cloudinary.com/diliidcs0/image/upload/c_scale,h_45,w_auto,q_100/v1595576634/1280px-Gojek_logo_2019.png" alt="Gojek Food" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </li>
)

export default Android;
