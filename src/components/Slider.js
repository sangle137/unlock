import React, { useRef, useState } from "react"
import { OverlayTrigger, Tooltip } from "react-bootstrap"
import { Icon } from '@iconify/react'

const classNames = require("classnames")

const Slider = ({ components }) => {
  const onNavClick = (e, index) => {
    e.preventDefault()
    setSelectedIndex(index)
  }

  const [selectedIndex, setSelectedIndex] = useState(0)
  const [startAnimating, setStartAnimating] = useState(false)

  const wrapperRef = useRef()
  const sliderRef = useRef()
  const navRefs = {}
  const onTransitionEnd = () => setStartAnimating(false)
  return (
    <div
      onTransitionEnd={onTransitionEnd}
      ref={wrapperRef}
      className={classNames("cd-slider-wrapper", { "slider-animating": startAnimating })}
    >
      <ul ref={sliderRef} className="cd-slider">
        {
          components.map(({ component: Component, slug }, idx) => {
            const cls = classNames({
              "cd-slide-item": true,
              "front-page front-page-countdown": idx === 0,
              "covered": idx < selectedIndex,
              "is-visible": idx === selectedIndex,
            });
            return <Component key={`slide-${idx}`} slug={slug} containerClass={cls} />
          })
        }
      </ul>
      <ol className="cd-slider-navigation">
        {components.length && components.map(({ icon, name, slug }, index) => {
          return (
            <OverlayTrigger
              key={slug}
              placement={"top"}
              overlay={
                <Tooltip
                  style={{ marginBottom: '10px', fontSize: '16px' }}
                >
                  {name}
                </Tooltip>
              }
            >
              <li
                ref={el => navRefs[index] = el}
                onClick={e => onNavClick(e, index)}
                className={`${index === selectedIndex ? "selected" : ""}`}
              >
                <a className="nav-tooltip tooltipstered" href={`#${index}`}>
                  <Icon icon={icon} width="23" height="23" />
                </a>
              </li>
            </OverlayTrigger>
          )
        })}
      </ol>
    </div>
  )
}

export default Slider
