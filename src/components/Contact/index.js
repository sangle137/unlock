import React from "react";
import { Marker } from 'google-maps-react';
import { Icon } from '@iconify/react'

import GoogleMap from '../GoogleMap';
import * as styles from './index.module.scss';

export default ({ containerClass }) => (
  <li className={containerClass}>
    <div className="cd-half-block image bgImage">
      <div className="content-wrapper">
        <div className="content-inner">
          <div className={styles.Contact}>
            <section className={styles.Section}>
              <article className={styles.SectionItem}>
                <header className={styles.SectionHeader}>Contact</header>

                <h2 className={styles.SectionSubHeader}>Vui lòng liên hệ</h2>
                <p className={styles.SectionSubText}>
                  <h3>Sún Ka chuyên các dòng iPhone, Android</h3>
                  <quote>Xoá icloud ẩn không cần password / Apple ID</quote>
                  <quote>Đăng nhập App Store bằng Apple ID mới trên các dòng iPhone, iPad, iPod</quote>
                  <quote>Mở khoá iPhone hoặc tắt Find My iPhone/iPad/iPod touch không cần password</quote>
                </p>

                <ul className={styles.TextList}>
                  <li>
                    <a target="_blank" href="tel:0942777718">+84-942-777-718 <Icon icon={'ep:location'} /></a>
                  </li>
                  <li>35/15 St. Le Binh, W. 4, D. Tan Binh, HCMC <Icon icon={'clarity:phone-handset-line'} /></li>
                  <li>
                    <a target="_blank" href="mailto:sunka.vn@gmail.com">
                      sunka.vn@gmail.com <Icon icon="codicon:mail" />
                    </a>
                  </li>
                </ul>

                <span className={styles.Social}>
                  <iframe
                    title="Sún Ka"
                    src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Ffacebook.com%2Fsunka.vn&width=136&layout=button_count&action=like&size=small&share=true&height=46&appId=1137248553141671"
                    width="142"
                    height="46"
                    style={{ border: 'none', overflow: 'hidden' }}
                    scrolling="no"
                    frameBorder="0"
                    allowtransparency="true"
                    allow="encrypted-media"
                  />
                </span>
              </article>
            </section>
          </div>
        </div>
      </div>
    </div>
    <div className="cd-half-block content" data-bg-color="#D98C52">
      <GoogleMap zoom={16} lat={10.794530} lng={106.656742}>
        <Marker position={{ lat: 10.794530, lng: 106.656742 }} title="Sún Ka" />
      </GoogleMap>
    </div>
  </li>
);
