import React from 'react'

import * as cls from './index.module.scss';

const Iphone = ({ containerClass, slug }) => (
  <li className={containerClass}>
    <div className="cd-half-block image product-section product-layout-4">
      <div className="product-group">
        <div className="product-item">
          <div className="product-thumbnail bgImage" />
          <div className="product-details">
            <div className="price">{10}<span>Vnd</span></div>
            <div className="product-details-wrapper">
              <h3 className="product-title">Test</h3>
              <div className="product-desc" dangerouslySetInnerHTML={{ __html: '' }} />

              <div className="button button-sm button-border button-icon">
                <a target="_blank" href="https://www.now.vn/ho-chi-minh/sun-coffee-milk-tea">
                  <span className="typcn typcn-shopping-cart" />Đặt hàng
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="cd-half-block content">
      <div className={cls.contentWrapper}>
        <div className="content-inner">
          <div className="disable-vertical-align">
            <div className={cls.titleContainer}>
              <img className={cls.titleLogo} src="/images/apple.png" alt="logo" />
              <span className={cls.titleText}>Apple</span>
            </div>
            <div data-height="68" style={{ height: '68px' }} />
            <div className="half-section-subtitle font-uppercase" style={{ marginBottom: '15px', letterSpacing: '4px' }}>
              Unlock iPhone
            </div>
            <div className={cls.blockText}>
              <p>Unlock iCloud không cần mật khẩu / Apple ID</p>
              <p>Mở khoá iCloud khi bạn quên mật khẩu hoặc mua iPhone lock</p>
              <p>Đăng nhập App Store với Apple ID mới sau khi mở khoá trên iPhone/iPad/iPod touch</p>
              <p>Mở khoá Apple ID hoặc tắt Find My iPhone trên iPhone/iPad/iPod touch không cần mật khẩu</p>
              <p>Tắt âm thanh camera không cần tất toàn bộ âm lượng</p>
            </div>
            <div data-height="37" style={{ height: '37px' }} />
            <div className="half-section-subtitle font-uppercase" style={{ marginBottom: '15px', letterSpacing: '4px' }}>
              Mở khoá mật khẩu
            </div>
            <div className={cls.blockText}>
              <p>Xoá mật khẩu 4 số / 6 số, Touch ID hoặc Face ID trên iPhone/iPad/iPod touch</p>
              <p>Xoá mật khẩu mở máy</p>
              <p>Xoá Apple ID trên iPhone/iPad/iPod touch</p>
              <p>Xoá MDM trên iPhone</p>
              <p>Sữa iPhone/iPad/iPod touch bị khoá do không có iTunes hoặc iCloud</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </li>
)

export default Iphone;
