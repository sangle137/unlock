import React from 'react';

import * as styles from './Vide.module.scss';

const Vide = ({ url }) => (
  <div className={styles.Container}>
    <video className={styles.Video} autoPlay="autoplay" loop="loop" muted >
      <source src={url} type="video/mp4" />
      Your browser does not support the video tag.
    </video>
  </div>
);

export default Vide;
