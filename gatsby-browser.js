/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
import "bootstrap/scss/bootstrap.scss";

import "./static/scss/fonts/typicons.font/typicons.min.css";
import "./static/scss/fonts/font-awesome/css/font-awesome.min.css";
import "./static/scss/reset.scss";
import "./static/scss/animate.scss";
import "./static/scss/PointySlider.scss";
import "./static/scss/color.scss"
import "./static/scss/style.scss";
import "./static/scss/custom.scss";

export { default as wrapRootElement } from './src/wrapRootElement';