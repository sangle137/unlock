/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it
const path = require('path');
const Dotenv = require('dotenv-webpack');

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    node: {
      fs: 'empty'
    },
    plugins: [
      new Dotenv({
        path: path.join(__dirname, '.env'),
        systemvars: true
      })
    ]
  })
}