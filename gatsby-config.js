module.exports = {
  siteMetadata: {
    title: `Sún Ka`,
    author: {
      name: `Sang Le`,
      summary: `Chuyên mở khoá android và iphone chuyên nghiệp`,
    },
    description: `A blog demonstrating about frontend experience.`,
    siteUrl: `https://unlock.sunka.vn/`,
    social: {
      twitter: `sanglnv`,
      facebook: `sanglnv`,
      instagram: `sanglnv`,
    },
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Sún Ka`,
        short_name: `SunKa`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `static/favicon.ico`,
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        implementation: require("sass")
      },
    }
  ],
};
